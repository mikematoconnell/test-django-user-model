from django.shortcuts import redirect, render

from users.forms import UserCreateForm


# Create your views here.
def register(request):
    if request.method == "POST":
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("register")

    context = {"form": UserCreateForm}
    return render(request, 'register.html', context)
