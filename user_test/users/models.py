from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db.models import Q

# Create your models here.
class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        return self._create_user(email, password, **extra_fields)

# email does need to be set as unique to use it as USERNAME_FIELD
class User(AbstractUser):
    username = None
    email = models.EmailField(unique=True)
    is_deleted = models.BooleanField(default=False)

    USERNAME_FIELD ="email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return str(self.email)
    
    class Meta:
        constraints =[
            models.UniqueConstraint(
                fields=["email"],
                condition=Q(is_deleted=False),
                name="unique_active_email"
            )
        ]
