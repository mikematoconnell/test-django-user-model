import csv

from django.conf import settings
from django.core.management.base import BaseCommand

from users.models import User


class Command(BaseCommand):
    help = "Loads car data from CSV file"

    def handle(self, *args, **options):
        users = User.objects.exclude(email="hh@gmail.com")

        for user in users:
            user.is_deleted = True
            user.save()